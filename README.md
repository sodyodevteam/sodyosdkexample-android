## Sodyo SDK Example application ##

** SodyoSdkExample uses [SodyoSDK](https://bitbucket.org/sodyodevteam/sodyosdk-android.git)  to scan Sodyo markers **


# Usage #
1. Clone the repository into your local workspace and import it to AndroidStudio.


2. To update your credentials, create/edit the file gradle.properties at the project root directory.
Update your Bitbucket ```USERNAME``` & ```PASSWORD``` as shown below.  
In addition, set your ```SODYO_APP_KEY``` information. Your ```SODYO_APP_KEY``` can be found in the application provider module of your account in the Sodyo Portal.

The final result would look as follows:

```c
# Project-wide Gradle settings.

# IDE (e.g. Android Studio) users:
# Gradle settings configured through the IDE *will override*
# any settings specified in this file.

# For more details on how to configure your build environment visit
# http://www.gradle.org/docs/current/userguide/build_environment.html

# Specifies the JVM arguments used for the daemon process.
# The setting is particularly useful for tweaking memory settings.
# Default value: -Xmx10248m -XX:MaxPermSize=256m
 org.gradle.jvmargs=-Xmx2048m -XX:MaxPermSize=512m -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8

# When configured, Gradle will run in incubating parallel mode.
# This option should only be used with decoupled projects. More details, visit
# http://www.gradle.org/docs/current/userguide/multi_project_builds.html#sec:decoupled_projects
# org.gradle.parallel=true

USERNAME=sodyoltd@gmail.com
PASSWORD=sodyo@sdk

SODYO_APP_KEY={YOUR_APP_KEY}
```

**That's it, Compile and run!**


Author
-----------------------------------
Ron Yagur, ron@sodyo.com  
Michael Yavorovsky, michael@brainway.co.il


License
------------------------
SodyoSDK is available under [Sodyo License Agreement](https://bitbucket.org/sodyodevteam/sodyosdk-android/raw/HEAD/SodyoLicenseAgreement.pdf).