package com.sodyo.sdkexample;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sodyo.sdk.Sodyo;
import com.sodyo.sdk.SodyoInitCallback;
import com.sodyo.sdk.SodyoScannerActivity;
import com.sodyo.sdk.SodyoScannerCallback;

public class MainActivity extends Activity implements SodyoScannerCallback, SodyoInitCallback {

	private static final String TAG = "MainActivity";

	private static final int SODYO_SCANNER_REQUEST_CODE = 1111;

	private View mScanBtn;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mScanBtn = findViewById(R.id.scan_btn);


		mScanBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// start SodyoScanner activity
				Intent intent = new Intent(MainActivity.this, SodyoScannerActivity.class);
				startActivityForResult(intent, SODYO_SCANNER_REQUEST_CODE);
			}
		});

		// disable scan btn. the button will be enabled when init completes
		mScanBtn.setEnabled(false);


		// init Sodyo engine App
		Sodyo.init(this.getApplication(), getString(R.string.sodyo_app_key), this);
		Sodyo.getInstance().setSodyoScannerCallback(this);

		addCustomOverlay();
		Sodyo.setCustomAdLabel("aaa,bbb,ccc");

	}

	private void addCustomOverlay() {
		LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		LinearLayout linearLayout = new LinearLayout(getApplicationContext());
		linearLayout.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		linearLayout.setLayoutParams(layoutParams);
		TextView label = new TextView(getApplicationContext());
		label.setText("Hello World :]");
		label.setTextColor(Color.WHITE);
		LinearLayout.LayoutParams labelLayoutParam=new LinearLayout.LayoutParams(350,150);
		label.setLayoutParams(labelLayoutParam);
		linearLayout.addView(label);

		Sodyo.setOverlayView(linearLayout);
	}


	/**
	 * SodyoInitCallback implementation
	 */
	@Override
	public void onSodyoAppLoadSuccess() {
		String message = "onSodyoAppLoadSuccess";
		Log.i(TAG, message);

		// enable scan button once Sodyo SDK init complete
		mScanBtn.setEnabled(true);
	}

	/**
	 * SodyoInitCallback implementation
	 */
	@Override
	public void onSodyoAppLoadFailed(String error) {
		String message = "onSodyoAppLoadFailed. Error=\"" + error + "\"";
		Log.e(TAG, message);
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	}


	@Override
	public void sodyoError(Error error) {

	}


	/**
	 * SodyoScannerCallback implementation
	 */
	@Override
	public void onMarkerDetect(String markerType, String data, String error) {
		if (data == null)
			data = "null";

		String message;
		if (error == null) {
			message = "SodyoScannerCallback.onMarkerDetect markerType=" + markerType + ", data=\"" + data + "\"";
			Log.i(TAG, message);
			Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();


		} else {
			message = "SodyoScannerCallback.onMarkerDetect  data=\"" + data + "\" error=\"" + error + "\"";
			Log.e(TAG, message);
		}

	}



}
